
Jean Sibelius, nacido en 1865, es un emblema de la música finlandesa. A través de sus estudios y conexiones, como con Busoni, Sibelius dejó una marca indeleble en la música europea, siempre con un toque finlandés.
